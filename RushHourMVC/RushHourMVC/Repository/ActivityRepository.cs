﻿using RushHourMVC.DataAccess;
using RushHourMVC.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RushHourMVC.Repository
{
    public class ActivityRepository : BaseRepository<Activity>
    {
        public ActivityRepository(RushHourContext context)
        {
            this.Context = context;
        }

        public override void Save(Activity activity)
        {
            if (activity.Id == 0)
            {
                base.Create(activity);
            }
            else
            {
                base.Update(activity, item => item.Id == activity.Id);
            }
        }
    }
}