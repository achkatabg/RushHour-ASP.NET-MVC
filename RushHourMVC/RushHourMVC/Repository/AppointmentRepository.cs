﻿using RushHourMVC.DataAccess;
using RushHourMVC.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RushHourMVC.Repository
{
    public class AppointmentRepository:BaseRepository<Appointment>
    {
        public AppointmentRepository(RushHourContext context)
        {
            this.Context = context;
        }

        public Appointment DBCheckAppointmentExists(DateTime date)
        {
            Appointment appointment = base.GetAll(a => date==a.StartDateTime).FirstOrDefault();
            if(appointment!=null)
            {
                return appointment;
            }
            else
            {
                return null;
            }
        }
    }
}