﻿using RushHourMVC.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RushHourMVC.Repository
{
    public class UnitOfWork
    {
        private RushHourContext Context = new RushHourContext();

        private UserRepository userRepository;
        private ActivityRepository activityRepository;
        private AppointmentRepository appointmentRepository;

        public UserRepository UserRepository
        {
            get
            {
                if (this.userRepository == null)
                {
                    this.userRepository = new UserRepository(Context);
                }
                return userRepository;
            }
        }

        public ActivityRepository ActivityRepository
        {
            get
            {
                if (this.activityRepository == null)
                {
                    this.activityRepository = new ActivityRepository(Context);
                }
                return activityRepository;
            }
        }

        public AppointmentRepository AppointmentRepository
        {
            get
            {
                if (this.appointmentRepository == null)
                {
                    this.appointmentRepository = new AppointmentRepository(Context);
                }
                return appointmentRepository;
            }
        }

        public int Save()
        {
            return Context.SaveChanges();
        }
    }
}