﻿using RushHourMVC.DataAccess;
using RushHourMVC.Models;
using RushHourMVC.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RushHourMVC.Repository
{
    public class UserRepository:BaseRepository<User>
    {
        public UserRepository(RushHourContext context)
        {
            this.Context = context;
        }

        public override void Save(User user)
        {
            if (user.Id == 0)
            {
                base.Create(user);
            }
            else
            {
                base.Update(user, item => item.Id == user.Id);
            }
        }
       
        public void RegisterUser(User user,string password)
        {
            base.Create(user);
        }

        public User GetUserByEmailAndPassword(string email, string password)
        {
            User user = base.DBSet.FirstOrDefault(u => u.Email == email && u.Password==password);
            return user;
        }

        public User CheckUserByEmailAndName(string email,string name)
        {
            User user = base.GetAll(u => email == u.Email || name == u.Name).FirstOrDefault();
            if (user!=null)
            {
                return user;
            }
            else
            {
                return null;
            }
        }
    }
}