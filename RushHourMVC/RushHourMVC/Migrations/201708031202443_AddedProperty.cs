namespace RushHourMVC.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedProperty : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Appointments", "Status", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Appointments", "Status");
        }
    }
}
