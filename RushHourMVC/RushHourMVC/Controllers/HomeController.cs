﻿using PagedList;
using RushHourMVC.Helpers;
using RushHourMVC.Models;
using RushHourMVC.Repository;
using RushHourMVC.Services;
using RushHourMVC.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using System.Web;
using System.Web.Mvc;

namespace RushHourMVC.Controllers
{
    
    public class HomeController : Controller
    {
       
        #region Constructors and field's
        private UserService userService;
        private ActivityService activityService;
        private AppointmentService appointmentService;

        public HomeController()
        {
            UnitOfWork unitOfWork = new UnitOfWork();
            this.userService = new UserService(new ValidationDictionary(ModelState),unitOfWork);
            this.activityService = new ActivityService(new ValidationDictionary(ModelState),unitOfWork);
            this.appointmentService = new AppointmentService(new ValidationDictionary(ModelState),unitOfWork);
        }
        #endregion

        public ActionResult Index()
        {
            return View();
        }

        #region USER
        [HttpGet]
        
        public ActionResult Login()
        {
            LoginViewModel viewModel = new LoginViewModel();

            return View(viewModel);
        }

        [HttpPost]
        [ActionName("Login")]
        public ActionResult LoginPost(LoginViewModel viewModel)
        {
            if (ModelState.IsValid)
            {
                User dbUser = userService.GetUserByEmailAndPassword(viewModel.Email, viewModel.Password);
                bool isUserExists = dbUser != null;
                if (isUserExists)
                {
                    LoginUserSession.Current.SetCurrentUser(dbUser.Id, dbUser.Email, dbUser.IsAdministrator, dbUser.Name, dbUser.Phone);
                    if (userService.UserOrAdminValidation(dbUser))
                    {
                        return RedirectToAction("LoggedAdmin");
                    }
                    else
                    {
                        return RedirectToAction("LoggedUser");
                    }
                }
                else
                {
                    ModelState.AddModelError("", "Invalid username and/or password");
                }
            }
            return View();
        }

        [HttpGet]
        public ActionResult Register()
        {
            RegisterViewModel viewModel = new RegisterViewModel();

            return View(viewModel);
        }

        [HttpPost]
        [ActionName("Register")]
        public ActionResult RegisterPost(RegisterViewModel viewModel)
        {
            if (userService.ValidateRegistration(viewModel.Email, viewModel.Name))
            {
                User user = new User();
                viewModel.UpdateUser(user);
                userService.SaveUser(user);
            }
            else
            {
                return this.RedirectToErrorPage("This user, already exist's");
            }
            return RedirectToAction("Index");
        }

        public ActionResult LoggedAdmin()
        {
            return View();
        }

        public ActionResult LoggedUser()
        {
            return View();
        }

        public ActionResult Logout()
        {
            LoginUserSession.Current.Logout();

            return RedirectToAction("Index");
        }

        [HttpGet]
        public ActionResult ViewMyProfile()
        {
            return RedirectToAction("ViewProfile", new { id = LoginUserSession.Current.UserID });
        }
        
        [HttpGet]
        public ActionResult ViewProfile(int id = 0)
        {
            User user = userService.GetUserById(id);
            if (userService.DBUserValidate(user))
            { 
                if(userService.IsAuthorized(user))
                { 
                    UserProfileViewModel viewModel = new UserProfileViewModel();
                    viewModel.Email = user.Email;
                    viewModel.Name = user.Name;
                    viewModel.Phone = user.Phone;

                    return View(viewModel);
                }
                else
                {
                    return this.RedirectToErrorPage("Access Denied");
                }
            }
            else
            {
                return this.RedirectToErrorPage("Invalid user");
            }
        }

        [HttpGet]
        public ActionResult EditMyProfile()
        {
            return RedirectToAction("Edit", new { id = LoginUserSession.Current.UserID });
        }

        [HttpGet]
        public ActionResult Edit(int id = 0)
        { 
           User user = userService.GetUserById(id);
           if (userService.DBUserValidate(user))
            {
                if (userService.IsAuthorized(user))
                {
                    EditProfileViewModel viewModelEdit = new EditProfileViewModel(user);
                    viewModelEdit = new EditProfileViewModel(user);

                    return View(viewModelEdit);
                }
                else
                {
                    return this.RedirectToErrorPage("Access Denied");
                }
            }
            else
            {
                return this.RedirectToErrorPage("Invalid user");
            }
        }

        [HttpPost]
        public ActionResult EditPost(EditProfileViewModel viewModelEdit)
        {
            User user = userService.GetUserById(viewModelEdit.UserID);
            if (userService.DBUserValidate(user))
            { 
                user.Email = viewModelEdit.Email;
                user.Name = viewModelEdit.Name;
                user.Phone = viewModelEdit.Phone;
                user.IsAdministrator = viewModelEdit.AdminFlag;
                userService.SaveUser(user);

                LoginUserSession.Current.Name = viewModelEdit.Name;
            }
            if(LoginUserSession.Current.IsAdministrator)
            {
                return RedirectToAction("AllUsers");
            }
            else
            {
                return RedirectToAction("LoggedUser");
            }
            
        }
        
        #region Appointments 

        [HttpGet]
        public ActionResult AllAppointments(int page = 1, int pageSize = 10)
        {
            User user = userService.GetUserById(LoginUserSession.Current.UserID);

            if(!LoginUserSession.Current.IsAdministrator)
            {
                List<Appointment> appointments = appointmentService.GetAllAppointments().Where(x=>x.UserID==user.Id).ToList();
                PagedList<Appointment> model = new PagedList<Appointment>(appointments, page, pageSize);

                return View(model);
            }
            else
            {
                List<Appointment> appointments = appointmentService.GetAllAppointments();
                PagedList<Appointment> model = new PagedList<Appointment>(appointments, page, pageSize);

                return View(model);
            }  
        }

        [HttpGet]
        public ActionResult CreateAppointment()
        {
            AppointmentViewModel appointmentViewModel = new AppointmentViewModel();
            appointmentViewModel.CheckedActivities = activityService.GetAllActivities();
            appointmentViewModel.UserID = LoginUserSession.Current.UserID;

            return View(appointmentViewModel);
        }

        [HttpPost]
        [ActionName("CreateAppointment")]
        public ActionResult CreateAppointmentPost(AppointmentViewModel appointmentViewModel)
        {
            if(appointmentService.AppointmentValidationDate(appointmentViewModel.StartDateTime))
            {   
                Appointment appointment = new Appointment();
                appointment.StartDateTime = appointmentViewModel.StartDateTime;
                appointment.EndDateTime = appointmentViewModel.EndDateTime;
                appointment.Status = appointmentViewModel.Status;
                appointment.UserID = appointmentViewModel.UserID;
                appointment.Activities = activityService.GetAllActivities().Where(x => appointmentViewModel.ChoosenActivities.Contains(x.Id)).ToList();

                appointmentService.SaveAppointment(appointment);
            }
            else
            {
                return this.RedirectToErrorPage("This date already reserved ! ");
            }
            return RedirectToAction("AllAppointments");
        }

        public ActionResult CancelAppointment(int id=0)
        {
            Appointment appointment = appointmentService.GetAppointmentById(id);

            if (appointmentService.AppointmentValidation(appointment))
            {
                appointment.Status = false;
                appointmentService.SaveAppointment(appointment);
            }
            else
            {
                return this.RedirectToErrorPage("Invalid id");
            }
            return RedirectToAction("AllAppointments");
        }

        [HttpGet]
        public ActionResult ViewAppointment(int id = 0)
        {
            Appointment appointment =appointmentService.GetAppointmentById(id);
            if (appointmentService.AppointmentValidation(appointment))
            {
                if(appointmentService.IsUserAuthorized(appointment))
                {
                    AppointmentViewModel viewModel = new AppointmentViewModel();
                    viewModel.StartDateTime = appointment.StartDateTime;
                    viewModel.EndDateTime = appointment.EndDateTime;
                    viewModel.CheckedActivities = appointment.Activities;

                    return View(viewModel);
                }
                else
                {
                    return this.RedirectToErrorPage("Access Denied");
                }
            }
            else
            {
                return this.RedirectToErrorPage("Invalid appointment");
            }
        }

        public ActionResult DeleteAppointment(int id = 0)
        {
            Appointment appointment = appointmentService.GetAppointmentById(id);
            bool isDeleted = appointmentService.DeleteAppointmentById(id);
            if (isDeleted == false)
            {
                TempData["ErrorMessage"] = "Could not find a activity with ID = " + id;
            }
            else
            {
                TempData["Message"] = "The activity was deleted successfully";
            }
            return RedirectToAction("AllAppointments");
        }

        [HttpGet]
        public ActionResult EditAppointment(int id =0)
        {
            Appointment appointment = appointmentService.GetAppointmentById(id);

            if (appointmentService.AppointmentValidation(appointment))
            {
                if (appointmentService.IsUserAuthorized(appointment))
                {
                    AppointmentViewModel viewModelEdit = new AppointmentViewModel(appointment);
                    List<Activity> chosenActivities = viewModelEdit.CheckedActivities;
                    viewModelEdit.NotChosenActivities = activityService.GetAllActivities().Where(x => !chosenActivities.Select(a => a.Id).Contains(x.Id)).ToList();

                    return View(viewModelEdit);
                }
                else
                {
                    return this.RedirectToErrorPage("Access Denied");
                }
            }
            else
            {
                return this.RedirectToErrorPage("Invalid appointment");
            }

        }

        [HttpPost]
        [ActionName("EditAppointment")]
        public ActionResult EditAppointmentPost(AppointmentViewModel viewModelEdit)
        {
            if (ModelState.IsValid)
            {
                Appointment appointment =appointmentService.GetAppointmentById(viewModelEdit.AppointmentID);
                appointment.StartDateTime = viewModelEdit.StartDateTime;
                appointment.EndDateTime = viewModelEdit.EndDateTime;
                appointment.Status = viewModelEdit.Status;
                appointment.Activities.Clear();
                appointment.Activities = activityService.GetAllActivities().Where(x => viewModelEdit.ChoosenActivities.Contains(x.Id)).ToList();

                appointmentService.SaveAppointment(appointment);
            }
            return RedirectToAction("AllAppointments");
        }
        #endregion 
        #endregion

        #region Administration

        #region Users
        [HttpGet]
        public ActionResult AllUsers(int page = 1, int pageSize = 10)
        {
            List<User> users = userService.GetAllUsers();
            PagedList<User> model = new PagedList<User>(users, page, pageSize);

            return View(model);
        }

        [HttpGet]
        public ActionResult CreateUser()
        {
            User user = new User();
            CreateUserViewModel viewModel = new CreateUserViewModel(user);

            return View(viewModel);
        }

        [HttpPost]
        [ActionName("CreateUser")]
        public ActionResult CreateUserPost(CreateUserViewModel viewModel)
        {
            if (ModelState.IsValid)
            {
                User user = new User();
                viewModel.UpdateUser(user);
                userService.SaveUser(user);
            }
            return RedirectToAction("AllUsers");
        }

        public ActionResult DeleteUser(int id = 0)
        {
            User user = userService.GetUserById(id);
            bool isDeleted = userService.DeleteUserById(id);

            if (isDeleted == false)
            {
                TempData["ErrorMessage"] = "Could not find a user with ID = " + id;
            }
            else
            {
                TempData["Message"] = "The user was deleted successfully";
            }
            return RedirectToAction("AllUsers");
        }
        #endregion
        
        #region Activities
        
        [HttpGet]
        public ActionResult AllActivities(int page = 1, int pageSize = 10)
        {
            List<Activity> activities = activityService.GetAllActivities();
            PagedList<Activity> model = new PagedList<Activity>(activities, page, pageSize);

            return View(model);
        }

        [HttpGet]
        public ActionResult CreateActivity()
        {
            Activity activity = new Activity();
            CreateActivityViewModel viewModel = new CreateActivityViewModel(activity);

            return View(viewModel);
        }

        [HttpPost]
        [ActionName("CreateActivity")]
        public ActionResult CreateActivityPost(CreateActivityViewModel viewModel)
        {
            if (ModelState.IsValid)
            {
                Activity activity = new Activity();
                viewModel.UpdateActivity(activity);
                activityService.SaveActivity(activity);
            }
            return RedirectToAction("AllActivities");
        }

        [HttpGet]
        public ActionResult ViewActivity(int id = 0)
        {
            Activity activity = activityService.GetActivityById(id);
            if(activityService.ActivityValidation(activity))
            {
                CreateActivityViewModel viewModel = new CreateActivityViewModel();
                viewModel.Name = activity.Name;
                viewModel.Duration = activity.Duration;
                viewModel.Price = activity.Price;

                return View(viewModel);
            }
            else
            {
                return this.RedirectToErrorPage("Invalid activity");
            }
        }

        public ActionResult DeleteActivity(int id =0)
        {
            Activity activity = activityService.GetActivityById(id);
            bool isDeleted = activityService.DeleteActivityById(id);

            if (isDeleted == false)
            {
                TempData["ErrorMessage"] = "Could not find a activity with ID = " + id;
            }
            else
            {
                TempData["Message"] = "The activity was deleted successfully";
            }
            return RedirectToAction("AllActivities");
        }

        [HttpGet]
        public ActionResult EditActivity(int id = 0)
        {
            Activity activity = activityService.GetActivityById(id);
            if(activityService.ActivityValidation(activity))
            {
                CreateActivityViewModel viewModelEdit = new CreateActivityViewModel(activity);
                viewModelEdit = new CreateActivityViewModel(activity);

                return View(viewModelEdit);
            }
            else
            {
                return this.RedirectToErrorPage("Invalid activity");
            }
        }

        [HttpPost]
        [ActionName("EditActivity")]
        public ActionResult EditActivityPost(CreateActivityViewModel viewModelEdit)
        {
            if (ModelState.IsValid)
            { 
                Activity activity = activityService.GetActivityById(viewModelEdit.ActivityID);
                activity.Name = viewModelEdit.Name;
                activity.Duration = viewModelEdit.Duration;
                activity.Price = viewModelEdit.Price;
                activityService.SaveActivity(activity);
            }
            return RedirectToAction("AllActivities");
        }
        #endregion

        #endregion
    }
}