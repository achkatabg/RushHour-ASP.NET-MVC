﻿using RushHourMVC.Helpers;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace RushHourMVC.Models
{
    public class User : BaseModel
    {
       [Required(ErrorMessage ="Put email")]
       [Index(IsUnique =true)]
       [StringLength(30)]
       public string Email { get; set;}

       [Required]
       [DataType(DataType.Password)]
       [StringLength(30, MinimumLength = 6,ErrorMessage ="Your password must be 6 characters!")]
       public string Password { get; set;}

       [Required] 
       public string Name {get; set;}

       [StringLength(20, ErrorMessage = "Max length is 20 characters")]
       public string Phone { get; set;}

       public bool IsAdministrator { get; set;}
       public virtual List<Appointment> Appointments { get; set;}

       public User()
       {
            Appointments = new List<Appointment>();
       }
    }
}