﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RushHourMVC.Models
{
    public class Appointment : BaseModel
    {
        public DateTime StartDateTime { get; set; } = DateTime.Now;
        public DateTime EndDateTime { get; set; } = DateTime.Now;
        public bool Status { get; set; } = true;
        public int UserID { get; set; }
        public virtual User CreatedBy { get; set; }
        public virtual List<Activity> Activities { get; set; }

        public Appointment()
        {
            Activities = new List<Activity>();
        }
    }
}