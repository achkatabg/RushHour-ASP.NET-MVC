﻿using RushHourMVC.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace RushHourMVC.DataAccess
{
    public class RushHourContext : DbContext
    {
        public RushHourContext() : base("RushHourDB")
        {

        }
        public DbSet<User> Users { get; set; }
        public DbSet<Activity> Activities { get; set; }
        public DbSet<Appointment> Appointments { get; set; }
    }
}