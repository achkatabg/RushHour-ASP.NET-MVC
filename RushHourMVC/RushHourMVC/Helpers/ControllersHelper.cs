﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace RushHourMVC.Helpers
{
    public class ControllersHelper
    {

    }

    public static class ControllerExtensions
    {
        public static ViewResult RedirectToErrorPage(this Controller controller, string errorMessage, string errorDetails = null)
        {
            ViewResult  result = new ViewResult { ViewName = "ErrorPage" };
            result.ViewBag.ErrorMessage = errorMessage;
            result.ViewBag.ErrorDetails = errorDetails;

            return result;
        }
    }
}