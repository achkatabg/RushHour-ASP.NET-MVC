﻿using RushHourMVC.Helpers;
using RushHourMVC.Models;
using RushHourMVC.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RushHourMVC.Services
{
    public class UserService
    {
        private UnitOfWork unitOfWork;
        private ValidationDictionary validDictionary;

        public UserService(ValidationDictionary validationDictionary, UnitOfWork unitOfWork)
        {
            this.validDictionary = validationDictionary;
            this.unitOfWork = unitOfWork;
        }

        public User GetUserByEmailAndPassword(string email,string password)
        {
            return unitOfWork.UserRepository.GetUserByEmailAndPassword(email, password);
        }

        public List<User> GetAllUsers()
        {
            return unitOfWork.UserRepository.GetAll();

        }

        public User GetUserById(int id)
        {
            return unitOfWork.UserRepository.GetById(id);
        }

        public bool DBUserValidate(User user)
        {
            if (user != null)
            {
               return this.validDictionary.IsValid;
            }
            return false;
        }

        public bool IsAuthorized(User user)
        {
            bool isAdmin = LoginUserSession.Current.IsAdministrator;
            if (isAdmin || user.Id==LoginUserSession.Current.UserID)
            {
                return this.validDictionary.IsValid;
            }
            return false;
        }
       
        public bool UserOrAdminValidation(User user)
        {
           if(user.IsAdministrator==true)
            {
                return this.validDictionary.IsValid;
            }
            return false;
        }

        public bool ValidateRegistration(string email,string name)
        {
            User user =unitOfWork.UserRepository.CheckUserByEmailAndName(email, name);
            if (user==null)
            {
                return this.validDictionary.IsValid;
            }
            else
            {
                return false;
            }
        }

        public bool DeleteUserById(int id)
        {
            return unitOfWork.UserRepository.DeleteById(id);
        }

        public bool SaveUser(User user)
        {
            unitOfWork.UserRepository.Save(user);
            return unitOfWork.Save() > 0;
        }

    }
}