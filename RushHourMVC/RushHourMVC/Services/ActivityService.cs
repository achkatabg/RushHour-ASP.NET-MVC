﻿using RushHourMVC.Models;
using RushHourMVC.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RushHourMVC.Services
{
    public class ActivityService
    {
        private UnitOfWork unitOfWork;
        private ValidationDictionary validDictionary;

        public ActivityService(ValidationDictionary validDictionary,UnitOfWork unitOfWork)
        {
            this.validDictionary = validDictionary;
            this.unitOfWork = unitOfWork;
        }

        public List<Activity> GetAllActivities()
        {
            return unitOfWork.ActivityRepository.GetAll();
        }

        public Activity GetActivityById(int id)
        {
            return unitOfWork.ActivityRepository.GetById(id);
        }

        public bool ActivityValidation(Activity activity)
        {
            if (activity != null)
            {
                return this.validDictionary.IsValid;
            }
            return false;
        }

        public bool DeleteActivityById(int id)
        {
            unitOfWork.ActivityRepository.DeleteById(id);
            return unitOfWork.Save() > 0;
        }

        public bool SaveActivity(Activity activity)
        {
            unitOfWork.ActivityRepository.Save(activity);
            return unitOfWork.Save() > 0;
        }
    }
}