﻿using RushHourMVC.Helpers;
using RushHourMVC.Models;
using RushHourMVC.Repository;
using RushHourMVC.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RushHourMVC.Services
{
    public class AppointmentService
    {
        private UnitOfWork unitOfWork;
        private ValidationDictionary validDictionary;

        public AppointmentService(ValidationDictionary validationDictionary, UnitOfWork unitOfWork)
        {
            this.validDictionary = validationDictionary;
            this.unitOfWork = unitOfWork;
        }

        public List<Appointment> GetAllAppointments()
        {
            return unitOfWork.AppointmentRepository.GetAll();
        }

        public Appointment GetAppointmentById(int id)
        {
            return unitOfWork.AppointmentRepository.GetById(id);
        }

        public bool DeleteAppointmentById(int id)
        {
            return unitOfWork.AppointmentRepository.DeleteById(id);
        }

        public bool AppointmentValidation(Appointment appointment)
        {
            if(appointment != null)
            {
                return this.validDictionary.IsValid;
            }
            return false;
        }

        public bool IsUserAuthorized(Appointment appointment)
        {
            if(appointment.UserID==LoginUserSession.Current.UserID)
            {
                return this.validDictionary.IsValid;
            }
            return false;
        }

        public bool AppointmentValidationDate(DateTime date)
        {
            Appointment appointment = unitOfWork.AppointmentRepository.DBCheckAppointmentExists(date);
            if(appointment==null)
            {
                return this.validDictionary.IsValid;
            }
            else
            {
                return false;
            }
        }

        public bool SaveAppointment(Appointment appointment)
        {
            unitOfWork.AppointmentRepository.Save(appointment);
            unitOfWork.Save();
            return unitOfWork.Save() > 0;
        }
    }
}