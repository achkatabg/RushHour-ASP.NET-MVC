﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RushHourMVC.ViewModels
{
    public class UserProfileViewModel
    {
        public string Email { get; set; }
        public string Name { get; set; }
        public string Phone { get; set; }
    }
}