﻿using RushHourMVC.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace RushHourMVC.ViewModels
{
    public class CreateActivityViewModel
    {
        public int ActivityID { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        public float Duration { get; set; }

        [Required]
        public decimal Price { get; set; }

        public CreateActivityViewModel()
        {

        }

        public CreateActivityViewModel(Activity activity)
        {
            this.ActivityID = activity.Id;
            this.Name = activity.Name;
            this.Duration = activity.Duration;
            this.Price = activity.Price;
        }

        public void UpdateActivity(Activity activity)
        {
            activity.Name = this.Name;
            activity.Duration = this.Duration;
            activity.Price = this.Price;
        }
    }
}