﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace RushHourMVC.ViewModels
{
    public class ActivityViewModel
    {
        [Required]
        public string Name { get; set; }

        public float Duration { get; set; }
        public decimal Price { get; set; }
    }
}