﻿using RushHourMVC.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace RushHourMVC.ViewModels
{
    public class CreateUserViewModel
    {
        [Required]
        public string Email { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [StringLength(30, MinimumLength = 6, ErrorMessage = "Your password must be 6 characters!")]
        public string Password { get; set; }

        [Required]
        public string Name { get; set; }

        public string Phone { get; set;}

        public CreateUserViewModel()
        {

        }

        public CreateUserViewModel(User user)
        {
            this.Email = user.Email;
            this.Password = user.Password;
            this.Name = user.Name;
            this.Phone = user.Phone;
        }

        public void UpdateUser(User user)
        {
            user.Email = this.Email;
            user.Name = this.Name;
            user.Password = this.Password;
            user.Phone = this.Phone;
        }
    }
}