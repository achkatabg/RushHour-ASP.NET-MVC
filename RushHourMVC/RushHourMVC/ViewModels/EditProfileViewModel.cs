﻿using RushHourMVC.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace RushHourMVC.ViewModels
{
    public class EditProfileViewModel
    {
        public int UserID { get; set; }
        public string Email { get; set; }
        public string Name { get; set; }

        [StringLength(20, ErrorMessage = "Max length is 20 characters")]
        public string Phone { get; set; }

        public bool AdminFlag { get; set;}

        public EditProfileViewModel()
        {
        }

        public EditProfileViewModel(User user)
        {
            this.UserID = user.Id;
            this.Email = user.Email;
            this.Name = user.Name;
            this.Phone = user.Phone;
            this.AdminFlag = user.IsAdministrator;
        }

    }
}