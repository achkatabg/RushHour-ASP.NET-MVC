﻿using RushHourMVC.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace RushHourMVC.ViewModels
{
    public class RegisterViewModel 
    {
       [Required(ErrorMessage ="Put email")]
       [StringLength(30)]
       public string Email { get; set;}

       [Required]
       [DataType(DataType.Password)]
       [StringLength(30, MinimumLength = 6,ErrorMessage ="Your password must be 6 characters!")]
       public string Password { get; set;}

       [Required] 
       public string Name {get; set;}

       [StringLength(20,ErrorMessage ="Max length is 20 characters")]
       public string Phone { get; set;}


        public RegisterViewModel()
        {

        }

        public RegisterViewModel(User user)
        {
            this.Email = user.Email;
            this.Name = user.Name;
            this.Phone = user.Phone;
        }

        public void UpdateUser(User user)
        {
            user.Email = this.Email;
            user.Name = this.Name;
            user.Password = this.Password;
            user.Phone = this.Phone;
        }
    }
}