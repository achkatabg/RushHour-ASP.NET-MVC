﻿using RushHourMVC.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using RushHourMVC.Repository;

namespace RushHourMVC.ViewModels
{
    public class AppointmentViewModel
    {
        public DateTime StartDateTime { get; set; } = DateTime.Now;
        public DateTime EndDateTime { get; set; } = DateTime.Now;
        public bool Status { get; set; } = true;
        public int UserID { get; set; }
        public int AppointmentID { get; set; }
        public virtual User CreatedBy { get; set; }
        public  List<int> ChoosenActivities { get; set; }
        public virtual List<Activity> CheckedActivities { get; set; }
        public List<Activity> NotChosenActivities { get; set;}

        public AppointmentViewModel()
        {
            CheckedActivities = new List<Activity>();
            ChoosenActivities = new List<int>();
            NotChosenActivities = new List<Activity>();

        }

        public AppointmentViewModel(Appointment appointment)
        {
            this.StartDateTime = appointment.StartDateTime;
            this.EndDateTime = appointment.EndDateTime;
            this.Status = appointment.Status;
            this.UserID = appointment.UserID;
            this.AppointmentID = appointment.Id;
            this.ChoosenActivities = new List<int>();
            this.CheckedActivities = appointment.Activities;
        }
    }
}